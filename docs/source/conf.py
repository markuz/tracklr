# -*- coding: utf-8 -*-
from tracklr import Tracklr


project = "Tracklr"
copyright = "2007-2023, Marek Kuziel"
author = "Marek Kuziel"
version = Tracklr.__version__
release = Tracklr.__version__
extensions = ["sphinx.ext.autodoc"]
templates_path = ["_templates"]
source_suffix = ".rst"
master_doc = "index"
language = "en"
exclude_patterns = []
pygments_style = "bw"
html_theme = "furo"
html_static_path = ["_static"]
html_title = f"Tracklr {Tracklr.__version__}"
html_short_title = "Tracklr"
html_static_path = ["_static"]
html_show_sphinx = False
htmlhelp_basename = "TracklrDocs"
html_show_sourcelink = False
html_show_sphinx = True
man_pages = [
    (master_doc, "tracklr", f"Tracklr {Tracklr.__version__}",
     [author], 1)
]
