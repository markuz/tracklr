.. _usage:

Usage
=====

``tracklr -h`` or ``tracklr --help`` displays usage page.

Adding ``--debug`` option helps to debug issues during development.

``tracklr ls --help`` or ``tracklr pdf --help`` displays help pages for available commands.

Also see :ref:`philosophy`, :ref:`tracking` and :ref:`calendarsetups` information.


``init``
--------

``tracklr init {config,template}`` allows users to create ``tracklr.yml`` and ``pdf.html`` either
in the current working directory ie. where ``tracklr`` looks for these first, or
into so called user config directory eg. on Linux at ``~/.config/tracklr/``.

``tracklr`` looks for ``tracklr.yml`` and ``pdf.html`` firstly in the current working directory and
then in user config directory, or falls back to defaults.

By default ``tracklr`` uses its own configuration stored in ``Tracklr.__config__`` variable.

For PDF reports ``tracklr`` uses by default its own HTML template located in
``tracklr.pdf.Pdf.__template__`` variable.

Examples::

    # init tracklr.yml in current working dir
    tracklr init config

    # init pdf.html in user config dir
    tracklr init template --user-config-dir


``ls``
------

``tracklr ls`` lists all events in calendar and provides total number of hours tracked in the
calendar.

Example::

    tracklr ls -d 2019-02-1
    Tracklr - Command-line Productivity Toolset
    Total hours: 2.5
    Number of events: 2
    +------------+------------------+------------------------------------------------------------+-------+
    | Date       | Summary          | Description                                                | Hours |
    +------------+------------------+------------------------------------------------------------+-------+
    | 2019-02-16 | @Tracklr #v0.1.0 | Testing tracklr v0.1 with Ivan and Tomas                   |   0.5 |
    | 2019-02-16 | @Tracklr #v0.2.0 | * fixed timesheet sorting                                  |   2.0 |
    |            |                  | * fixed possible missing description/summary in ical feeds |       |
    |            |                  | * refactored logging                                       |       |
    |            |                  | * started on v0.2 ie. multi calendar support               |       |
    +------------+------------------+------------------------------------------------------------+-------+

More examples::

    # show only 2019 events
    tracklr ls -d 2019

    # show only 2019-02 events
    tracklr ls -d 2019-02

    # show only 2019 @tracklr events
    tracklr ls -d 2019 -i @tracklr

    # show only 2019 `other` calendar @project events
    tracklr ls -d 2019 -k other -i @project

    # show all 2019 events from `calendar` for @client's +project that matches #tag
    tracklr ls -d 2019 -k calendar -i @client +project "#tag"

    # show all March 2019 events without the ones matching #tags tag
    tracklr ls -d 2019-03 -x "#tags"


When ``-g``/``--group`` argument is used then ``ls`` output will contain found matches for the
given group key.

Examples::

    # filter out and display $ sum from the demo calendar events
    tracklr ls -d 2020-04 -g "$"
    Tracklr v1.4.0

    Title: Tracklr
    Subtitle: Command-line Productivity Toolset
    Configuration: default

    Total hours: 2.0
    Number of events: 2

    Total sum: 200.0

    +------------+-----------------------+-------------+-------+-------+
    | Date       | Summary               | Description | Hours |     $ |
    +------------+-----------------------+-------------+-------+-------+
    | 2020-04-15 | @Tracklr #v1.4.0 $100 |             |   1.0 | 100.0 |
    | 2020-04-15 | @Tracklr #v1.4.0 $100 |             |   1.0 | 100.0 |
    +------------+-----------------------+-------------+-------+-------+


``pdf``
-------

``tracklr pdf`` creates PDF report which displays date, description and hours.

Example::

    # generate 2019 @tracklr PDF report 
    tracklr pdf -d 2019 -i @tracklr

    # generate 2019 @tracklr PDF report using custom html template
    tracklr pdf -d 2019 -i @tracklr -e custom_report.html

    # generate 2019 @tracklr PDF report using custom html template and custom output file
    tracklr pdf -d 2019 -i @tracklr -e custom_report.html -f ~/Downloads/custom_report.pdf


See example file here: :download:`report.pdf <_static/report.pdf>`.


``info``
--------

``tracklr info`` displays information about the current instance.


``group``
---------

``tracklr group`` creates accumulative reports of events of which summaries match given ``key``
pattern using the folloring regexp:

.. code:: python

    matches = re.compile(r"\{}([a-zA-Z0-9_\.]+)".format(key))


By default the key using for finding matches is ``#``.

The default group key can be changed by specifying ``-g GROUP``, or ``--group GROUP`` argument.

Example of default ``#`` grouping::

    tracklr group -g "#"         
    ___ ____ ____ ____ _  _ _    ____ 
     |  |__/ |__| |    |_/  |    |__/ 
     |  |  \ |  | |___ | \_ |___ |  \ v1.3.0

    Title: Tracklr
    Subtitle: Command-line Productivity Toolset
    Configuration: default

    Total hours: 47.0

    +--------+-------+
    | Group  | Hours |
    +--------+-------+
    | v0.1.0 | 4.5   |
    | v0.2.0 | 3.5   |
    | v0.3.0 | 2.0   |
    | v0.4.0 | 2.0   |
    | v0.5.0 | 2.0   |
    | v0.6.0 | 2.0   |
    | v0.7.0 | 6.0   |
    | v0.8.0 | 2.0   |
    | v0.9.0 | 1.0   |
    | v0.9.1 | 0.5   |
    | v0.9.2 | 1.0   |
    | v0.9.3 | 2.5   |
    | v0.9.4 | 1.0   |
    | v0.9.5 | 2.0   |
    | v1.0.0 | 4.0   |
    | v1.0.1 | 2.0   |
    | v1.1.0 | 2.0   |
    | v1.1.1 | 2.0   |
    | v1.1.2 | 1.0   |
    | v1.1.3 | 2.0   |
    | v1.2.0 | 2.0   |
    +--------+-------+


Example of grouping by ``@``::

    tracklr group -g "@"
    ___ ____ ____ ____ _  _ _    ____ 
     |  |__/ |__| |    |_/  |    |__/ 
     |  |  \ |  | |___ | \_ |___ |  \ v1.3.0

    Title: Tracklr
    Subtitle: Command-line Productivity Toolset
    Configuration: default

    Total hours: 47.0

    +---------+-------+
    | Group   | Hours |
    +---------+-------+
    | Tracklr | 47.0  |
    +---------+-------+


``add``
-------

``tracklr add --name "Example Event" --description "Example Description"`` creates new events in calendars.

It was introduced in ``v1.6.0``. It only works for calendars with an URL as their location protected with username and password.

It does not work for local ``vdir`` calendars and calendars without authentication.

Id of the new event is displayed on success, otherwise ``0`` is displayed.

Example::

    tracklr add -n test                                                                                                                                                                                    ✹ ✭
    .-----.                .-.   .-.       
    `-. .-'                : :.-.: :       
      : :.--.  .--.   .--. : `'.': :  .--. 
      : :: ..'' .; ; '  ..': . `.: :_ : ..'
      :_;:_;  `.__,_;`.__.':_;:_;`.__;:_; v1.6.0
    
    Title: Tracklr
    Subtitle: Command-line Productivity Toolset
    Configuration: default
    id:40abd873-5132-12ee-3b20-623e52ad8c78
