Tracklr
=======

    Command-line Productivity Toolset

Tracklr loads and processes calendar events from `iCalendar` feeds to produce reports for you.

.. toctree::
   :maxdepth: 1

   README
   configuration
   usage
   philosophy
   tracking
   calendar-setups
   development
   api
   CHANGELOG


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
