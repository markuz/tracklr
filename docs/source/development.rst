Development
===========

To develop ``tracklr`` via ``pyenv`` + ``virtualenv``, run::

    git clone https://gitlab.com/markuz/tracklr    # clone tracklr repo
    cd tracklr                                     # get into the repo dir
    pyenv virtualenv 3.11.1 tracklr                # create new virtualenv
    source ~/.pyenv/versions/tracklr/bin/activate  # activate new env
    make -f local.mk update                        # install dependencies
    make -f local.mk develop                       # build development egg
    make -f local.mk docs                          # generate documentation
    make -f local.mk test                          # run testing suite
