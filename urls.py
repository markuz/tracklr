"""Django urls file used for development of ``Tracklr REST API``.
"""
from django.urls import path
from django.urls import re_path

from rest_framework import permissions

from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from tracklr_rest_api.views import LsView

schema_view = get_schema_view(
   openapi.Info(
      title="Tracklr REST API",
      default_version="v1",
      description="All features of Tracklr served as a set of REST APIs",
      terms_of_service="https://tracklr.com/terms/",
      contact=openapi.Contact(email="marek@kuziel.info"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path(
        "ls",
        LsView.as_view(),
        name="ls"
    ),
   re_path(r"^openapi(?P<format>\.json|\.yaml)$", schema_view.without_ui(cache_timeout=0), name="schema-json"),
   re_path(r"^openapi/$", schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger-ui"),
]
