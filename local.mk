.PHONY: help_local
help_local:
	@echo "~~~~~~~~~~~~"
	@echo "tracklr help"
	@echo "~~~~~~~~~~~~"
	@echo "help_local  - this help"
	@echo "update      - pip install -U -r requirements/development.txt"
	@echo "develop     - work on tracklr"
	@echo "sdist       - package up"
	@echo "upload_test - package up & upload release to test pypi"
	@echo "upload      - package up & upload release to pypi"
	@echo "clean       - remove build artifacts"
	@echo "report      - generate demo report for documentation"
	@echo "docs        - generate HTML documentation"
	@echo "push        - push to gits"
	@echo "test        - run tests with coverage report"
	@echo "coverage    - uploads coverage report to coverage.io"
	@echo "black       - black the code"

.PHONY: update
update:
	@pip install -U -r requirements/development.txt

.PHONY: develop
develop:
	python setup.py develop

.PHONY: sdist
sdist:
	python setup.py sdist bdist_wheel

.PHONY: upload_test
upload_test:
	twine upload --repository-url https://test.pypi.org/legacy/ dist/*

.PHONY: upload
upload:
	twine upload dist/*

.PHONY: clean
clean:
	rm -rf dist build *.egg-info
	cd docs && make clean

.PHONY: report
report:
	tracklr pdf -f docs/source/_static/report.pdf

.PHONY: docs
docs:
	cd docs && make clean
	cd docs && make html

.PHONY: push
push:
	git push origin master
	git push --tags origin master

.PHONY: test
test:
	@pytest --cov=tracklr tests
	@coverage report -m
	@coverage xml

.PHONY: coverage
coverage:
	@curl -s https://codecov.io/bash > codecovio.sh
	bash codecovio.sh -t @.cc_token

.PHONY: black
black:
	@black -l 80 tests tracklr

.PHONY: backup_local
backup_local:
	@touch .last_backup

.PHONY: restore_local
restore_local:
	@touch .last_restore
