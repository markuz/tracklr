CHANGELOG
=========

2023-09-26 | v1.6.1
-------------------

* Fixed getting the correct calendar ie. not the first one from the principal object for adding new calendar events.

2023-09-20 | v1.6.0
-------------------

* Added ``tracklr add --name "Example Event" --description "Example Description"`` command which creates new events in calendars. It only works for remote calendars ie. have URL as their location and are protected with username and password.

2022-04-20 | v1.5.4
-------------------

* Tidy-up release


2021-04-12 | v1.5.3
-------------------

* Removed custom logging and added more tests
* Finished off first full version of ls REST API view
* Added back wsgi.py for development of tracklr rest api
* Added docstrings into manage, settings and urls
* Reorganized tracklr rest api files
* Removed unused tracklr rest api projects files; Updated docs; Ignored .tracklr.yml and db.sqlite3 files; Added ls REST API endpoint and openapi spec url endpoints
* Updated development docs
* Tidy up changelog

2020-05-19 | v1.5.2
-------------------

* Tidy up release

2020-05-19 | v1.5.1
-------------------

* Tidy up release

2020-05-08 | v1.5.0
-------------------

* Removed Python 2 support
* Removed deprecated code from v1.0.0 for old location and timezone handling
* Added development and productivity requirements
* Added tests and coverage report
* Added codecov.io setup
* Updated documentation
* Improved grouping parser

2020-04-30 | v1.4.2
-------------------

* Removed common groups so that all groups in ls and group commands are in the same Group column for consistency.
* Added handler for identifying 100,000.00 as numbers in parsed groups
* Changed event summary parser so that it not only parses ``#hashtags``, ``$monies``, ``@something`` but
  also ``23g``, ``1024hPa``,  ``30C`` types of groups.

2020-04-30 | v1.4.1
-------------------

* Change default group for ``pdf`` command to be ``#`` ie. retain previous default behaviour
* Changed default ``pdf`` template to generate list of found group matches
* Return found group matches consistently as a list not as a list or a string


2020-04-15 | v1.4.0
-------------------

* Changed implementation of ``-g`` ``--group`` argument. It now has no default so that ``ls`` and
  ``group`` commands can implemented their own handling of what the default should be.
* ``ls`` command now adds a new column with found matches for given ``--group`` key. This allows to
  extract things such as currency eg. ``--group "$"``.
* ``group`` command now defaults to ``#`` key ie. previous behaviour of matching and grouping by
  hashtags is preserved.
* The following group keys are recognized by ``Tracklr`` and displayed as column title:
  ``"~", "!", "@", "#", "$", "%", "^", "&", "*"``


2020-01-05 | v1.3.1
-------------------

* Changed pdf command implementation from tag to group.
* Replace _ with space in default PDF HTML template.
* Updated tracking docs.


2020-01-05 | v1.3.0
-------------------

* **DEPRECATED** ``tag`` command.
* **NEW** ``group`` command allows to group by any key defaulting to ``#``
  ie. by default ``group`` command provides the same behaviour
  as the old ``tag`` command.

2020-01-04 | v1.2.0
-------------------

* Added ability to specify html template file for pdf reports so that other than
  ``pdf.html`` templates can be used.


2020-01-04 | v1.1.3
-------------------

* Split Tracklr's ``get_parser`` to ``get_base_parser`` so that ``init`` and ``info``
  commands can use ``--kalendar``, ``--title`` and ``--subtitle`` args only
  ie. base args shared among all commands. This also fixes ``init`` command not working.


2020-01-01 | v1.1.2
-------------------

* Fixed ``make report`` command
* Fixed title retrieval for pdf command
* Updated PDF report for docs


2020-01-01 | v1.1.1
-------------------

* Removed dependency on ``pyfiglet``
* Fixed vdir not found exception handling
* Fixed Info command
* Fixed entrypoint for the new info command

2020-01-01 | v1.1.0
-------------------

* ``show`` command is now ``info``
* All commands now use new tracklr info banner
* Added simple vdir object that provides list and get functions ie. removed dependency on ``khal``
* Changed package classifiers and added ``tracklr v1.0.1`` to pypi


2019-12-30 | v1.0.1
-------------------

* Changed vdir location handling so that `~/` is expanded to full path ie. `~/.calendars/...`
  locations can now be used in the config
* Added handler for when the given location of vdir does not exist


2019-12-27 | v1.0.0
-------------------

* Added vdir support using `Khal's <https://khal.readthedocs.io/en/latest/>`_ ``Vdir`` class.
* **DEPRECATED** ``url`` option in the config. Use ``location`` for either URLs or vdir paths.
* **DEPRECATED** ``x_wr_timezone`` option in the config. Use ``timezone`` and either set it to
  ``True`` for ``X-WR-TIMEZONE`` feeds (eg. Google Calendar), or specify your feed's timzone
  manually. Eg. ``Pacific/Auckland``.


2019-12-24 | v0.9.5
-------------------

* Configuration can be now also stored in ``.tracklr.yml`` file
* Added tracklr banner and version number to show command
* Documented addition of images to HTML templates for PDF reports


2019-11-20 | v0.9.4
-------------------

* Added `Number of events` to ``ls`` log info output

2019-06-10 | v0.9.3
-------------------

* Fix for event filter; abort filtering if there is no summary info in event

2019-06-10 | v0.9.2
-------------------

* Tag parser now also includes all untagged hours using #untagged tag
* If more than one tag is used per event the hours are then split evenly between the tags

2019-06-10 | v0.9.1
-------------------

* Updated internal pdf template to add ``<br>`` tag only if there is an event description
  before any parsed tags are displayed.


2019-06-04 | v0.9.0
-------------------

* Display tags from event summaries in PDF reports


2019-05-04 | v0.8.0
-------------------

* Added support for ``X-WR-TIMEZONE`` - thanks William Buick for the report and the patch
* Switched to `semantic versioning <https://semver.org/>`_


2019-03-30 | v0.7.0
-------------------

* Refactored the code base so that it is more generic (no more client, project, tag) include and exclude patterns
* Exclude patterns now can be used for filtering
* New internal pdf report
* Tracklr pdf --report {ls, tag} can be now used to choose between ls and tag report in pdf format
* Documentation updates


2019-03-02 | v0.6.0
-------------------

* Added ``tracklr tag`` command
* New default pdf html template
* Added sorting of html report


2019-02-24 | v0.5.0
-------------------

* Added ``tracklr show`` command to display information about ``tracklr`` configuration
* More flexible calendar configuration options for simpler setup


2019-02-23 | v0.4.0
-------------------

* Added ``tracklr init {config,template}`` command so that users can create ``tracklr.yml`` and
  ``pdf.html`` either in current working directory or in user config directory. 
* Fixed title/subtitle in ls and pdf commands
* Renamed timesheet to report
* Refactored config and pdf template to be loaded first from local dir, then user config dir or
  locally from Tracklr demo ie. defaults
* One version setting via ``Tracklr.__version__`` to rule them all


2019-02-22 | v0.3.0
-------------------

* Finished off imlpementation to support multiple calendars
* Updated documentation


2019-02-20 | v0.2.0
-------------------

* ``tracklr.yml`` config changed; users can use multiple calendars. See docs for details.
* Fixed missing ``DESCRIPTION`` in events. Thanks Ivan Müller for the report.


2019-02-09 | v0.1.0
-------------------

* Initial version
